## Analyzing the Created Project

* manage, py - use but don't change - in my page directory
* wsgi.py & asgi.py use at Project deployment
* __init.py__ used for Python / Django housekeeping
*  settings.py - will edit eventually but defaults are OK to start
* urls.py - used to define urls (web pages) of our site

### Starting a development server

* in sub directory containing manage.py `python3 manage.py runserver`
* runs at localhost:8000

### 14. Django apps 

* django projects consist of 1 or more apps - you work in app subfolders
* Create app `python3 manage.py startapp challenges`
* creates challenges directory same level as my page 

### 15 Analyzing Created challenges app

* migration folder - used later for databases 
* admin.py - works with built in admin 
* apps.py - ok for now 
* models.py - used later for databases
* test.py - for automated testing 
* views.py - Super important
