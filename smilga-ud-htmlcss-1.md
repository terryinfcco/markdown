# John Smilga HTML/CSS Udemy P. 1 

* Using vscode & Google Chrome 
* .config VSCodium/User/settings.json ==> settings
* Some extensions require editing json file directly 
* Created folder Sync/Smilga/html 

### 10. 1st Web Page 

* create index.html ==> home page always named this
* hl = heading 1 largest heading 
* p = paragraph 
* circle on file name tab in VsCode means unsaved 

### 11. Basic HTML Document Structure 

* Doctype - HTML version 
* html - wraps all of page 
* head - contained completely inside html tags (contents not visible on web page) 
* body - contained completely inside html tags (contents visible on web page)  

### 12. Implement Document Structure 

* Ctrl-B Show & hide sidebar 

### 13. LIVE SERVER Extension 

* Live Server by Ritwick Dey 
* Right click on html page in vscode and select open with Live server

### 14. Extra Settings & Emmet 

* He enables Editor Word Wrap 
* p then tab key - paragraph tags 
* start typing element name & see drop down list or press tab 
* ! tab key - basic HTML 5 Page Structure 

### 15 Heading Elements 

* 6 sizes h1-h6 where you want text to stand out. 

### 17 Paragraph Elements 
* Regular text `<p> </p>` 

### 18 HTML ignores white space 

### 19 Lorem Ipsum 
* Placeholder text - lorem tab key 
* lorem 25 tab key 25 words 
* lorem 500 tab key 500 words 
