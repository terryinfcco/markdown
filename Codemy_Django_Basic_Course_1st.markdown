### Building a very simple website with no database, just very basic stuff.

* Create a subdirectory called and move to it.

``` bash
 mkdir code
 cd code
```

* Set up and activate a virtual environment:

``` bash
python3 -m venv code
source code/bin/activate
```

* Install Django - he's on version 2.0.4 and I'm going to follow along

* Create code/djangoproject and cd  to it.

``` bash
mkdir djangoproject
cd djangoproject
```

* I created bash alias dj1 to go to djangoproject with the venv activated.

* Now create the django project: 

``` bash
django-admin.py startproject mysite .
```
In later versions of django I think this is just django-admin. 

That didn't work - I would have to go back to python 3.7 or earlier according to some stuff I read on the web. So I uninstalled the old version of Django and installed 4.0.1. We'll see what blows up next.

``` bash
django-admin startproject mysite .
```

* In mysite sub-directory
    * __init__.py - empty file needed by django
    * settings.py - will be used
    * urls.py - create urls here
    * wsgi.py - behind scenes
    * asgi.py - not mentioned in video
    
* Now run the local server to test our installation

``` bash
python manage.py runserver
```
Should have a screen at 127.0.0.1:8000 that says the installation worked. You get an error message about unapplied migratoins. It's about databases, which we're not using in this tutorial. You can get rid of this by running:

``` bash
python manage.py migrate
```
This also creates an admin url in the mysite/urls.py file.

* Now create the first user

``` bash
python manage.py createsuperuser
```
At this point you can get into the admin area at 127.0.0.1/admin. I created user terry with password jacquidj 

* Create a Django app. Django apps are pieces of the website.

``` bash
python manage.py startapp pages
```
* Setup urls by copying urls.py from mysite to pages
* Add `'pages',` to the end of  INSTALLED_APPS list in mysite/settings.py 
* Finally change urls.py in pages subdirectory to:

``` python
from django.urls import path

urlpatterns = [ ]
```






