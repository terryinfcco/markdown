## Python Django - The Practical Guide - Academind
### Udemy course

* Django 3.1.5 -Should be close enough to current which is 3.29

## Section 1 
### Course prequisites

* Basic Python
* HTML, CSS - BASIC Knowledge 

### Course Design
* Built to be taken in order.
* Foundation - Setup, Urls, routes, views, templates, data models 
* Beyond the Basics-forms, class based views, sessions
* Preparing for Reality - Examples, Build Blog, Deployment 
* Watch videos, code along & code ahead where applicable. 
* Use the code attachments 
* Summary module at end good for future review (4 hrs) 

## Section 2 Setup & create Django Projects

### Install Django

* can install globally or in a virtual environment
* he recommends installing globally. 
* Runs django-admin to make sure django installed ok. `pip install django` 

### Create a new Django project 

* go to folder that will contain Django projects
* `django-admin start project mypage` 
* Install IDE -Visual Studio Code
* install python extension & pylance, make sure python version selected in bottom status bar.
* ctrl-Shift- I to format Python code
