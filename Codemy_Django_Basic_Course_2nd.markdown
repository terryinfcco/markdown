### Codemy Django Intro Second Markdown File
#### This is where we start creating code

#### Video 10

* Edit pages/views.py and add after the comment

``` python
def home(request):
	return render(request, "home.html", {})
def about(request):
	return render(request, "about.html", {})
```

#### Video 11

* Create the template files:
    * Create pages/templates subdirectory
    * Create home.html and about.html. Put an H1 in each with About Page and Home Page respectively.
    
#### Video 12

* in pages/urls.py add:

``` python
from . import views
urlpatterns = [
	path('', views.home, name='home'),
	path('about/', views.about, name='about')
	]
```

* in mysite/urls.py 
    * add include to import path statement.
    * add in urlpatterns list:
        * ```path('', include('pages.urls')),```
* At this point run the server and you've got home.html at the root of the site and at /about you've got about.html.

