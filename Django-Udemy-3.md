## Udemy Django Academind p.3 
### Section 3 URL'S & Views 
### 18 

* URLs called routes in Django
- Request & response is the basic web model
- Use challenges app to demo delivering content & processing input 

### 19 creating new project

* `django-admin start project monthly-challenges`
* python3 manage.py start app challenges - new app 

### 20 URL's (Routes), views

* most websites have multiple pages (URL'S / Routes)
    * my-page, com - start page 
    * my-page.com / posts - blog index
    * my-page.com/tutonals - list of tutorials 

* Views are actions that get triggered when a URL is selected
* views are python functions, could be a class 
* evaluates request, returns response
    * load & prepare data 
    * run business logic 
    * return response data (HTML/CSS)
